import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  
  recipes;
  
  constructor(private api: ApiService) {  }

  ngOnInit() {
   this.recipes = this.api.getRecipes();
   this.recipes = [
    new Recipe('Amatriciana', 'L\'Amatriciana è un tipico piatto italiano', 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Bucatini_allamatriciana.jpg/250px-Bucatini_allamatriciana.jpg'),
    new Recipe('Carbonara', 'La Carbonara è un tipico piatto italiano', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Spaghetti_alla_Carbonara_%28cropped%29.jpg/260px-Spaghetti_alla_Carbonara_%28cropped%29.jpg')
  ];
  }

  getRecipes() {
    this.api.getRecipes()
      .subscribe(data => {
        for (const d of (data as any)) {
          this.recipes.push({
            name: d.name,
            description: d.description,
            imagePath: d.imagePath
          });
        }
        console.log(this.recipes);
      });
  }

}
